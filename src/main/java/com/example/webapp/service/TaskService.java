package com.example.webapp.service;


import com.example.webapp.model.Task;
import com.example.webapp.repository.TaskRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TaskService {
    @Autowired
    TaskRepository taskRepo;

    public List<Task> getAllTasks() {
        return taskRepo.findAll();
    }

    public Task getTaskById(Integer id) {
        Optional<Task> taskById = taskRepo.findById(id);
        if (taskById.isPresent()) {
            return taskById.get();
        }
        return new Task();
    }


    public String addTask(Task task) {
        taskRepo.save(task);
        return "Successfully created!!";
    }

    public String editTask(Integer id, Task task) {
        List<Task> taskList = taskRepo.findAll();
        for (Task currentTask : taskList) {
            if (currentTask.getId() == id) {
                currentTask.setTitle(task.getTitle());
                currentTask.setDescription(task.getDescription());
                taskRepo.save(currentTask);
                return "Successfully edited!!!";
            }
        }
        return "Nask not found";
    }

    public String deleteTaskById(Integer id) {
        try {
            taskRepo.deleteById(id);
            return "Successfully deleted!";
        } catch (Exception e) {
            return "Task not found!";
        }
    }
}
