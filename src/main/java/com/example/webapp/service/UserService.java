package com.example.webapp.service;


import com.example.webapp.exception.UserNotFoundException;
import com.example.webapp.model.User;
import com.example.webapp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired private UserRepository userRepo;


    public List<User> listAll()
    {
        return (List<User>) userRepo.findAll();
    }



    public void save(User user)
    {
        userRepo.save(user);
    }

    public User getId(Integer id) throws UserNotFoundException
    {
        Optional<User> result = userRepo.findById(id);
        if(result.isPresent())
        {
            return result.get();
        }

        throw new UserNotFoundException("User not fount this id " + id);

    }

    public void userDelete(Integer id) throws UserNotFoundException
    {
        Long count = userRepo.countById(id);
        if (count==null||count==0)
        {
            throw new UserNotFoundException("User not fount this id " + id);
        }
        userRepo.deleteById(id);
    }
}
