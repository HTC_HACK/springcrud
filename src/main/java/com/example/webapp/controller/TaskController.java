package com.example.webapp.controller;


import com.example.webapp.model.Task;
import com.example.webapp.service.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/tasks")
@CrossOrigin
public class TaskController {

    @Autowired
    TaskService taskService;

    @GetMapping
    public List<Task> getAll() {
        return taskService.getAllTasks();
    }

    @GetMapping("/{id}")

    public Task showTask(Integer id) {
        return taskService.getTaskById(id);
    }

    @PostMapping

    public String createTask(@RequestBody Task task) {
        return taskService.addTask(task);
    }

    @PutMapping("/{id}")

    public String editTask(@PathVariable Integer id, @RequestBody Task task) {
        return taskService.editTask(id, task);
    }

    @DeleteMapping("/{id}")
    public String deleteTask(@PathVariable Integer id) {
        return taskService.deleteTaskById(id);
    }
}
