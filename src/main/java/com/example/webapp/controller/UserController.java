package com.example.webapp.controller;

import com.example.webapp.exception.UserNotFoundException;
import com.example.webapp.model.User;
import com.example.webapp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class UserController {

    @Autowired
    private UserService service;

    @GetMapping("/")
    public String showHomePage(Model model) {

        List<User> listUsers = service.listAll();

        model.addAttribute("listUsers", listUsers);

        return "index";
    }

    @GetMapping("/users/create")
    public String createUser(Model model) {
        model.addAttribute("user", new User());
        model.addAttribute("title","User Create");
        return "userAction/create";
    }

    @PostMapping("/users/save")
    public String userStore(User user, RedirectAttributes ra) {
        service.save(user);
        ra.addFlashAttribute("message", "User successfully saved");

        return "redirect:/";
    }

    @GetMapping("/users/edit/{id}")
    public String userEdit(@PathVariable("id") Integer id, Model model, RedirectAttributes ra) {
        try {
            User user = service.getId(id);
            model.addAttribute("user", user);
            model.addAttribute("title","User edit " + id + "");
            return "userAction/create";

        } catch (UserNotFoundException e) {
            ra.addFlashAttribute("message", e.getMessage());
            return "redirect:/";
        }

    }

    @GetMapping ("/users/delete/{id}")
    public String userDelete(@PathVariable("id") Integer id, RedirectAttributes ra) {
        try {
            service.userDelete(id);
            ra.addFlashAttribute("message", "User successfully deleted " + id);
            return "redirect:/";

        } catch (UserNotFoundException e) {
            ra.addFlashAttribute("message", e.getMessage());
            return "redirect:/";
        }


    }


}
